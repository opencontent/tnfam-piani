'use strict';

const express = require('express');
const morgan = require('morgan');

const app = express();

const HOST = '0.0.0.0';
const PORT = 3000;

if (app.get('env') == 'production') {
  // use combined preset, see https://github.com/expressjs/morgan#combined
  app.use(morgan('combined'));
} else {
  // use dev preset, more suited to development
  app.use(morgan('dev'));
}

app.use(express.static('public'));
app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
