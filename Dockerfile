FROM node:10.15.0-alpine

LABEL maintainer="lorenzo.salvadorini@opencontent.it"

WORKDIR /srv

COPY package*.json ./
RUN npm install --quiet --only=prod
COPY . .

EXPOSE 3000

RUN apk add --no-cache tini
# Tini is now available at /sbin/tini
ENTRYPOINT ["/sbin/tini", "--"]

# Run your program under Tini
CMD [ "node", "index.js" ]
